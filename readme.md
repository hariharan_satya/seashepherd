SeaShepherd Donations is written in ASP.NET Core 2.0 
Tools Used
1. RestSharp
2. XUnit
3. Moq

eWay API used and things done - 
1. Used eWay's Secure Fields - https://eway.io/api-v3/?_ga=2.40725860.710735912.1528268651-1224150991.1527041103#secure-fields
2. Used eWay's Direct Connection - https://eway.io/api-v3/?_ga=2.40725860.710735912.1528268651-1224150991.1527041103#direct-connection
3. Didn't focus much on UI or UI Validation
4. Hosted website in AWS Elastic Beanstalk which can be accessed at http://seashepherddonations.ap-southeast-2.elasticbeanstalk.com/

Should have done (but didn't have time to do)
1. Used https/ ssl
2. More unit test / integration test to improve coverage
