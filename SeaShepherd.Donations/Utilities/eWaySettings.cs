﻿namespace SeaShepherd.Donations.Utilities
{
    public class eWaySettings : IeWaySettings
    {
        public string SandboxBaseUri { get; set; }
        public string SandboxAPIKey { get; set; }
        public string SanboxAPIPassword { get; set; }
        public string PublicAPIKey { get; set; }
    }
}
