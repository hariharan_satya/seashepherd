﻿namespace SeaShepherd.Donations.Utilities
{
    public interface IeWaySettings
    {
        string PublicAPIKey { get; set; }
        string SanboxAPIPassword { get; set; }
        string SandboxAPIKey { get; set; }
        string SandboxBaseUri { get; set; }
    }
}