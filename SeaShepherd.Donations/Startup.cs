﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SeaShepherd.Donations.APIClient;
using SeaShepherd.Donations.Payments;
using SeaShepherd.Donations.Services;
using SeaShepherd.Donations.Utilities;

namespace SeaShepherd.Donations
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<eWaySettings>(Configuration.GetSection("eWay"));
            
            var sp = services.BuildServiceProvider();
            var ewayOptions =  sp.GetService<IOptions<eWaySettings>>().Value;
            services.AddTransient<IeWaySettings, eWaySettings>(x=> ewayOptions);
            // setup DI
            services.AddTransient<IDonationService, DonationService>();
            services.AddTransient<IDirectTransactionService, DirectTransactionService>();
            services.AddTransient<IApiClient, ApiClient>();
            services.AddMvc();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
