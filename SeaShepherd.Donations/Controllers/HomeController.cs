﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SeaShepherd.Donations.Models;
using SeaShepherd.Donations.Services;

namespace SeaShepherd.Donations.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDonationService _donationService;
        private readonly ILogger _logger;

        public HomeController(IDonationService donationService,ILogger<HomeController> logger)
        {
            _donationService = donationService;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View(new DonationViewModel());
        }

        public IActionResult ProcessDonation(DonationViewModel donationViewModel)
        {
            DonationResultViewModel result;
            try
            {
                result = _donationService.ProcessDonation(donationViewModel);
            }
            catch (Exception e)
            {
                _logger.LogError("Unhandled exception occured while processing donation",e);
                throw;
            }
            
            return View("DonationResult",result);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
