﻿using System.Net;
using SeaShepherd.Donations.APIClient;
using SeaShepherd.Donations.Models;
using SeaShepherd.Donations.Payments;
using SeaShepherd.Donations.Payments.Models;
using CardDetails = SeaShepherd.Donations.Payments.CardDetails;
using Customer = SeaShepherd.Donations.Payments.Customer;
using Payment = SeaShepherd.Donations.Payments.Payment;

namespace SeaShepherd.Donations.Mapping
{
    public static class Mappings
    {
        public static Transaction ToTransaction(this DonationViewModel donationViewModel)
        {
            var transaction = new Transaction
            {
                TransactionType = TransactionType.Purchase,
                Method = PaymentMethod.ProcessPayment,
                Customer = new Customer
                {
                    Title = donationViewModel.Title,
                    FirstName = donationViewModel.FirstName,
                    LastName = donationViewModel.LastName,
                    CardDetails = new CardDetails
                    {
                        CVN = donationViewModel.CVN,
                        ExpiryMonth = donationViewModel.ExpiryMonth,
                        ExpiryYear = donationViewModel.ExpiryYear,
                        Name = donationViewModel.CardName,
                        Number = donationViewModel.CardNumber,
                    }
                },
                Payment = new Payment
                {
                    TotalAmount = donationViewModel.DonationAmount
                },
            };

            return transaction;
        }

        public static Transaction ToSecureCardDataTransaction(this DonationViewModel donationViewModel)
        {
            var transaction = new Transaction
            {
                TransactionType = TransactionType.Purchase,
                Method = PaymentMethod.ProcessPayment,
                SecuredCardData = donationViewModel.SecuredCardData,
                Customer = new Customer
                {
                    Title = donationViewModel.Title,
                    FirstName = donationViewModel.FirstName,
                    LastName = donationViewModel.LastName
                },
                Payment = new Payment
                {
                    TotalAmount = donationViewModel.DonationAmount
                },
            };

            return transaction;
        }

        public static DonationResultViewModel ToDonationResult(this IDirectTransactionResult directTransactionResult)
        {
            return new DonationResultViewModel
            {
                TransactionId = directTransactionResult.TransactionId,
                ResponseMessage = directTransactionResult.ResponseMessage,
                IsSuccess = directTransactionResult.IsSuccess
            };
        }

        public static IDirectTransactionResult ToDirectTransactionResult(
            this IApiClientResult<TransactionResponse> transactionResponseResult)
        {
            var httpStatusCode = transactionResponseResult.StatusCode;
            var responseData = transactionResponseResult.ResponseData;

            var result = new DirectTransactionResult
            {
                HttpStatusCode = httpStatusCode,
                IsSuccess = false
            };

            if (httpStatusCode != HttpStatusCode.OK)
            {
                result.ResponseMessage = "Unknown system error occured during processing of transaction.";
                return result;
            }

            if (responseData.ResponseMessage != null && responseData.ResponseMessage.StartsWith("A"))
            {
                result.TransactionId = responseData.TransactionID.ToString();
                result.ResponseMessage = $"Transaction was successful. Transaction Id is {result.TransactionId}";
                result.IsSuccess = true;
                return result;
            }

            var errorCode = responseData.Errors ?? responseData.ResponseMessage;
            result.ResponseMessage =
                $"Transaction was unsuccessful. Transaction failed with error code(s) {errorCode}";
            return result;

        }
    }
}
