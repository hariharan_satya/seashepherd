﻿using System.Net;

namespace SeaShepherd.Donations.APIClient
{
    public interface IApiClientResult<T> where T : new()
    {
        T ResponseData { get; set; }
        HttpStatusCode StatusCode { get; set; }
    }
}