﻿using RestSharp;
using SeaShepherd.Donations.Utilities;
using System;
using System.Net;
using System.Text;

namespace SeaShepherd.Donations.APIClient
{
    public class ApiClient : IApiClient
    {
        private readonly IeWaySettings _ewaySettings;
        public ApiClient(IeWaySettings ewaySettings)
        {
            _ewaySettings = ewaySettings;
        }

        private string AuthorizationHeader => $"Basic {Convert.ToBase64String(Encoding.ASCII.GetBytes($"{_ewaySettings.SandboxAPIKey}:{_ewaySettings.SanboxAPIPassword}"))}";

        public IApiClientResult<T> ExecutePost<T>(string body) where T : new()
        {
            var client = new RestClient($"{_ewaySettings.SandboxBaseUri}/Transaction");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Authorization", AuthorizationHeader);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("undefined", body, ParameterType.RequestBody);

            var response = client.Execute<T>(request);
            return response.StatusCode == HttpStatusCode.OK ? 
                new ApiClientResult<T> { StatusCode = response.StatusCode, ResponseData = response.Data } : 
                new ApiClientResult<T> { StatusCode = response.StatusCode, ResponseData = default(T) };
        }
    }

    public class ApiClientResult<T> : IApiClientResult<T> where T : new()
    {
        public HttpStatusCode StatusCode { get; set; }
        public T ResponseData { get; set; }
    }
}
