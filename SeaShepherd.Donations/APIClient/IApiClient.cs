﻿namespace SeaShepherd.Donations.APIClient
{
    public interface IApiClient
    {
        IApiClientResult<T> ExecutePost<T>(string body) where T : new();
    }
}