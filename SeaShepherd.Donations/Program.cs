﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace SeaShepherd.Donations
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.
                CreateDefaultBuilder(args)
                .ConfigureLogging((ctx,logging)=> logging.AddDebug())
                .UseStartup<Startup>()
                .Build();
    }
}
