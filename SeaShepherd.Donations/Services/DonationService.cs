﻿using SeaShepherd.Donations.Mapping;
using SeaShepherd.Donations.Models;
using SeaShepherd.Donations.Payments;

namespace SeaShepherd.Donations.Services
{
    public class DonationService : IDonationService
    {
        private readonly IDirectTransactionService _directTransactionService;

        public DonationService(IDirectTransactionService directTransactionService)
        {
            _directTransactionService = directTransactionService;
        }
        public DonationResultViewModel ProcessDonation(DonationViewModel donationViewModel)
        {
            var result = _directTransactionService.CreateTransaction(donationViewModel.ToSecureCardDataTransaction());

            return result.ToDonationResult();
        }
    }
}
