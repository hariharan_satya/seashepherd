﻿using SeaShepherd.Donations.Models;

namespace SeaShepherd.Donations.Services
{
    public interface IDonationService
    {
        DonationResultViewModel ProcessDonation(DonationViewModel donationViewModel);
    }
}