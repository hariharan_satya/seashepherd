﻿namespace SeaShepherd.Donations.Payments
{
    public interface IDirectTransactionService
    {
        IDirectTransactionResult CreateTransaction(Transaction transaction);
    }
}