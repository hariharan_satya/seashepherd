﻿using SeaShepherd.Donations.APIClient;
using SeaShepherd.Donations.Mapping;
using SeaShepherd.Donations.Payments.Models;

namespace SeaShepherd.Donations.Payments
{
    public class DirectTransactionService : IDirectTransactionService
    {
        private readonly IApiClient _apiClient;

        public DirectTransactionService(IApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        public IDirectTransactionResult CreateTransaction(Transaction transaction)
        {
            var result = _apiClient.ExecutePost<TransactionResponse>(transaction.ToString());
            return result.ToDirectTransactionResult();
        }
    }
}
