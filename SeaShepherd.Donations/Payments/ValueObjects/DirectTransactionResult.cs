﻿using System.Net;

namespace SeaShepherd.Donations.Payments
{
    public class DirectTransactionResult : IDirectTransactionResult
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public string ResponseMessage { get; set; }
        public string TransactionId { get; set; }
        public bool IsSuccess { get; set; }
        
        //internal static IDirectTransactionResult Create(HttpStatusCode statusCode, TransactionResponse responseData)
        //{
        //    var result = new DirectTransactionResult
        //    {
        //        HttpStatusCode = statusCode,
        //        IsSuccess = false
        //    };

        //    if (statusCode != HttpStatusCode.OK)
        //    {
        //        result.ResponseMessage = "Error processing transaction";
        //        return result;
        //    }

        //    if (responseData.ResponseMessage.StartsWith("A"))
        //    {
        //        result.TransactionId = responseData.TransactionID.ToString();
        //        result.ResponseMessage = $"Transaction was successful. Transaction Id is {result.TransactionId}";
        //        result.IsSuccess = true;
        //        return result;
        //    }

        //    var errorCode = responseData.Errors ?? responseData.ResponseMessage;
        //    result.ResponseMessage =
        //        $"Transaction was unsuccessful. Transaction failed with error code(s) {errorCode}";
        //    return result;
        //}
    }
}