﻿using System.Net;

namespace SeaShepherd.Donations.Payments
{
    public interface IDirectTransactionResult
    {
        HttpStatusCode HttpStatusCode { get; set; }
        string ResponseMessage { get; set; }
        string TransactionId { get; set; }
        bool IsSuccess { get; set; }
    }
}