﻿namespace SeaShepherd.Donations.Payments
{
    public class Customer
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public CardDetails CardDetails { get; set; }
    }
}
