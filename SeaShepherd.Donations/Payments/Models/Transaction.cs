﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SeaShepherd.Donations.Utilities;

namespace SeaShepherd.Donations.Payments
{
    public partial class Transaction
    {
        public string SecuredCardData { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public PaymentMethod Method { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public TransactionType TransactionType { get; set; }
        public Customer Customer { get; set; }
        public Payment Payment { get; set; }
    }

    public partial class Transaction
    {
        public static Transaction FromJson(string json) => JsonConvert.DeserializeObject<Transaction>(json, JsonConverterSettings.Settings);
        private string ToJson() => JsonConvert.SerializeObject(this, JsonConverterSettings.Settings);

        public bool ShouldSerializeCardDetails() => SecuredCardData == null || (Customer?.CardDetails != null);

        public bool ShouldSerializeSecuredCardData() => !string.IsNullOrEmpty(SecuredCardData);

        /// <inheritdoc />
        public override string ToString()
        {
            return ToJson();
        }
    }

    public enum TransactionType
    {
        Purchase, MOTO, Recurring
    }

    public enum PaymentMethod
    {
        ProcessPayment, CreateTokenCustomer, UpdateTokenCustomer, TokenPayment, Authorise
    }
}
