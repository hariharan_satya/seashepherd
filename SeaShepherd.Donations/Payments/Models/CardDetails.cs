﻿namespace SeaShepherd.Donations.Payments
{
    public class CardDetails
    {
        public string Name { get; set; }
        public string Number { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
        public string CVN { get; set; }
    }
}
