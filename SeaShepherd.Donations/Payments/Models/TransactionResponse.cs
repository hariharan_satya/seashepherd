﻿using Newtonsoft.Json;
using SeaShepherd.Donations.Utilities;

namespace SeaShepherd.Donations.Payments.Models
{
    public partial class TransactionResponse
    {
        [JsonProperty("AuthorisationCode")]
        public string AuthorisationCode { get; set; }

        [JsonProperty("ResponseCode")]
        public string ResponseCode { get; set; }

        [JsonProperty("ResponseMessage")]
        public string ResponseMessage { get; set; }

        [JsonProperty("TransactionID")]
        public long TransactionID { get; set; }

        [JsonProperty("TransactionStatus")]
        public bool TransactionStatus { get; set; }

        [JsonProperty("TransactionType")]
        public string TransactionType { get; set; }

        [JsonProperty("BeagleScore")]
        public long BeagleScore { get; set; }

        [JsonProperty("Verification")]
        public Verification Verification { get; set; }

        [JsonProperty("Customer")]
        public Customer Customer { get; set; }

        [JsonProperty("Payment")]
        public Payment Payment { get; set; }

        [JsonProperty("Errors")]
        public string Errors { get; set; }
    }

    public partial class Customer
    {
        [JsonProperty("CardDetails")]
        public CardDetails CardDetails { get; set; }

        [JsonProperty("TokenCustomerID")]
        public object TokenCustomerId { get; set; }

        [JsonProperty("Reference")]
        public string Reference { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("CompanyName")]
        public string CompanyName { get; set; }

        [JsonProperty("JobDescription")]
        public string JobDescription { get; set; }

        [JsonProperty("Street1")]
        public string Street1 { get; set; }

        [JsonProperty("Street2")]
        public string Street2 { get; set; }

        [JsonProperty("City")]
        public string City { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("PostalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("Country")]
        public string Country { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("Comments")]
        public string Comments { get; set; }

        [JsonProperty("Fax")]
        public string Fax { get; set; }

        [JsonProperty("Url")]
        public string Url { get; set; }
    }

    public partial class CardDetails
    {
        [JsonProperty("Number")]
        public string Number { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("ExpiryMonth")]
        public string ExpiryMonth { get; set; }

        [JsonProperty("ExpiryYear")]
        public string ExpiryYear { get; set; }

        [JsonProperty("StartMonth")]
        public object StartMonth { get; set; }

        [JsonProperty("StartYear")]
        public object StartYear { get; set; }

        [JsonProperty("IssueNumber")]
        public object IssueNumber { get; set; }
    }

    public class Payment
    {
        [JsonProperty("TotalAmount")]
        public long TotalAmount { get; set; }

        [JsonProperty("InvoiceNumber")]
        public object InvoiceNumber { get; set; }

        [JsonProperty("InvoiceDescription")]
        public object InvoiceDescription { get; set; }

        [JsonProperty("InvoiceReference")]
        public object InvoiceReference { get; set; }

        [JsonProperty("CurrencyCode")]
        public string CurrencyCode { get; set; }
    }

    public partial class Verification
    {
        [JsonProperty("CVN")]
        public long Cvn { get; set; }

        [JsonProperty("Address")]
        public long Address { get; set; }

        [JsonProperty("Email")]
        public long Email { get; set; }

        [JsonProperty("Mobile")]
        public long Mobile { get; set; }

        [JsonProperty("Phone")]
        public long Phone { get; set; }
    }

    public partial class TransactionResponse
    {
        public static TransactionResponse FromJson(string json) => JsonConvert.DeserializeObject<TransactionResponse>(json, JsonConverterSettings.Settings);
        private string ToJson() => JsonConvert.SerializeObject(this, JsonConverterSettings.Settings);

        /// <inheritdoc />
        public override string ToString()
        {
            return ToJson();
        }
    }
}
