﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SeaShepherd.Donations.Models
{
    public class DonationViewModel
    {
        public decimal DonationAmount { get; set; }

        public List<SelectListItem> TitleList => new List<SelectListItem>
        {
            new SelectListItem{Text = "Mr." , Value = "Mr." },
            new SelectListItem{Text = "Ms." , Value = "Ms." },
            new SelectListItem{Text = "Mrs." , Value = "Mrs." },
            new SelectListItem{Text = "Miss." , Value = "Miss." },
            new SelectListItem{Text = "Dr." , Value = "Dr." },
            new SelectListItem{Text = "Prof." , Value = "Prof." },
        };
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CardNumber { get; set; }
        public string CVN { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
        public string CardName { get; set; }
        public string SecuredCardData { get; set; }
    }
}
