﻿namespace SeaShepherd.Donations.Models
{
    public class DonationResultViewModel
    {
        public string ResponseMessage { get; set; }
        public string TransactionId { get; set; }
        public bool IsSuccess { get; set; }
    }
}
