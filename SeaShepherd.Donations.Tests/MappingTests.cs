using System.Net;
using FluentAssertions;
using Moq;
using SeaShepherd.Donations.APIClient;
using SeaShepherd.Donations.Mapping;
using SeaShepherd.Donations.Payments;
using SeaShepherd.Donations.Payments.Models;
using Xunit;
using CardDetails = SeaShepherd.Donations.Payments.CardDetails;
using Customer = SeaShepherd.Donations.Payments.Customer;
using Payment = SeaShepherd.Donations.Payments.Payment;

namespace SeaShepherd.Donations.Tests
{
    public class MappingTests : TestsBase
    {

        [Fact]
        public void ToTransaction_should_map_DonationViewModel_to_Transaction()
        {
            var transactionResult = UnsecuredTransaction;
            transactionResult.SecuredCardData.Should().Be(UnsecuredDonationViewModel.SecuredCardData);
            transactionResult.TransactionType.Should().Be(TransactionType.Purchase);
            transactionResult.Method.Should().Be(PaymentMethod.ProcessPayment);
            transactionResult.Customer.Should().BeEquivalentTo(new Customer
            {
                Title = UnsecuredDonationViewModel.Title, FirstName = UnsecuredDonationViewModel.FirstName, LastName = UnsecuredDonationViewModel.LastName, CardDetails = new CardDetails
                {
                    CVN = UnsecuredDonationViewModel.CVN,
                    ExpiryYear = UnsecuredDonationViewModel.ExpiryYear,
                    ExpiryMonth = UnsecuredDonationViewModel.ExpiryMonth,
                    Number = UnsecuredDonationViewModel.CardNumber,
                    Name = UnsecuredDonationViewModel.CardName
                }

            });
            transactionResult.Payment.Should().BeEquivalentTo(new Payment
            {
                TotalAmount = UnsecuredDonationViewModel.DonationAmount
            });
        }

        [Fact]
        public void Unsecured_Transaction_ToString_should_convert_to_JSON_representation_enum_value_must_be_converted_to_string_value()
        {
            var serialisedTransaction = UnsecuredTransaction.ToString();
            serialisedTransaction.Should().Contain("\"Method\":\"ProcessPayment\"");
            serialisedTransaction.Should().Contain("\"TransactionType\":\"Purchase\"");
        }
        
        [Fact]
        public void Unsecured_Transaction_ToString_should_convert_toJSON_and_should_not_contain_SecuredCardData()
        {
            var serialisedTransaction = UnsecuredTransaction.ToString();
            serialisedTransaction.Should().NotContain("\"SecuredCardData\":\"xxxx\"");
        }

        [Fact]
        public void Secured_Transaction_ToString_should_convert_to_JSON_and_should_not_contain_SecuredCardData()
        {
            var serialisedTransaction = SecuredTransaction.ToString();
            serialisedTransaction.Should().Contain("\"SecuredCardData\":\"secured\"");
        }

        [Fact]
        public void DirectTransactionResult_When_TransactionResponse_is_successful_DirectTransactionResult_IsSuccess_should_be_true()
        {
            const long transactionId = 999999L;
            var transactionResponseMock = new Mock<IApiClientResult<TransactionResponse>>();
            transactionResponseMock.Setup(x => x.StatusCode).Returns(HttpStatusCode.OK);
            transactionResponseMock.Setup(x => x.ResponseData)
                .Returns(new TransactionResponse {ResponseMessage = "A2000",TransactionID = transactionId });
            var directTransactionResult = transactionResponseMock.Object.ToDirectTransactionResult();
            directTransactionResult.IsSuccess.Should().BeTrue();
            directTransactionResult.TransactionId.Should().Be(transactionId.ToString());
            directTransactionResult.HttpStatusCode.Should().Be(HttpStatusCode.OK);
            directTransactionResult.ResponseMessage.Should()
                .Be($"Transaction was successful. Transaction Id is {directTransactionResult.TransactionId}");
        }

        [Fact]
        public void DirectTransactionResult_When_TransactionResponse_is_not_successful_DirectTransactionResult_IsSuccess_should_be_false()
        {
            var transactionResponseMock = new Mock<IApiClientResult<TransactionResponse>>();
            transactionResponseMock.Setup(x => x.StatusCode).Returns(HttpStatusCode.OK);
            transactionResponseMock.Setup(x => x.ResponseData)
                .Returns(new TransactionResponse { ResponseMessage = "D2000" });
            var directTransactionResult = transactionResponseMock.Object.ToDirectTransactionResult();
            directTransactionResult.IsSuccess.Should().BeFalse();
            directTransactionResult.HttpStatusCode.Should().Be(HttpStatusCode.OK);
            directTransactionResult.ResponseMessage.Should()
                .Be($"Transaction was unsuccessful. Transaction failed with error code(s) {transactionResponseMock.Object.ResponseData.ResponseMessage}");
        }

        [Fact]
        public void DirectTransactionResult_When_TransactionResponse_is_not_successful_and_returns_with_a_validation_error_DirectTransactionResult_IsSuccess_should_be_false()
        {
            var transactionResponseMock = new Mock<IApiClientResult<TransactionResponse>>();
            transactionResponseMock.Setup(x => x.StatusCode).Returns(HttpStatusCode.OK);
            transactionResponseMock.Setup(x => x.ResponseData)
                .Returns(new TransactionResponse { Errors = "V6101,V6102,V6103" });
            var directTransactionResult = transactionResponseMock.Object.ToDirectTransactionResult();
            directTransactionResult.IsSuccess.Should().BeFalse();
            directTransactionResult.HttpStatusCode.Should().Be(HttpStatusCode.OK);
            directTransactionResult.ResponseMessage.Should()
                .Be($"Transaction was unsuccessful. Transaction failed with error code(s) {transactionResponseMock.Object.ResponseData.Errors}");
        }
    }
}
