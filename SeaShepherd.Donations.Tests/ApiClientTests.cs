﻿using FluentAssertions;
using SeaShepherd.Donations.APIClient;
using SeaShepherd.Donations.Payments.Models;
using Xunit;

namespace SeaShepherd.Donations.Tests
{
    public class ApiClientTests : TestsBase
    {
        private readonly ApiClient _apiClient;

        public ApiClientTests()
        {
            _apiClient = new ApiClient(EWaySettings);
        }
        [Fact]
        public void ExecutePost_Transaction_with_card_details_sample_JSON_must_return_successful_response()
        {
            var response = _apiClient.ExecutePost<TransactionResponse>(TransationSample);
            response.ResponseData.ResponseMessage.Should().Be("A2000");
        }

        [Fact]
        public void ExecutePost_Transaction_with_card_details_must_return_successful_response()
        {
            var response = _apiClient.ExecutePost<TransactionResponse>(UnsecuredTransaction.ToString());
            response.ResponseData.ResponseMessage.Should().Be("A2000");
        }

        [Fact]
        public void ExecutePost_Transaction_with_card_details_when_amount_is_invalid_must_return_error_response()
        {
            var unsecuredTransaction = UnsecuredTransaction;
            unsecuredTransaction.Payment.TotalAmount = -1000;
            var response = _apiClient.ExecutePost<TransactionResponse>(unsecuredTransaction.ToString());
            response.ResponseData.ResponseMessage.Should().NotBe("A2000");
            response.ResponseData.Errors.Should().Contain("V6011");
        }
    }
}
