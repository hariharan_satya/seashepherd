﻿using Moq;
using SeaShepherd.Donations.Mapping;
using SeaShepherd.Donations.Models;
using SeaShepherd.Donations.Payments;
using SeaShepherd.Donations.Utilities;

namespace SeaShepherd.Donations.Tests
{
    public class TestsBase
    {
        public IeWaySettings EWaySettings
        {
            get
            {
                var ewaySettings = new Mock<IeWaySettings>();
                ewaySettings.Setup(x => x.PublicAPIKey).Returns("epk-9C8A2FB7-13FB-44AB-B1B0-A90CAF5A6813");
                ewaySettings.Setup(x => x.SanboxAPIPassword).Returns("Abcd1234");
                ewaySettings.Setup(x => x.SandboxAPIKey).Returns("F9802CL6tBYFeInWkTdR/ZTnii9NgHEz7fZNVCrWf0FSTnfXlMBxdM0lgJD5QSYSoOqUlI");
                ewaySettings.Setup(x => x.SandboxBaseUri).Returns("https://api.sandbox.ewaypayments.com");
                return ewaySettings.Object;
            }
        }

        protected DonationViewModel UnsecuredDonationViewModel => new DonationViewModel
        {
            // customer
            Title = "Mr.",
            FirstName = "Hariharan",
            LastName = "Satyavakeeswaran",
            MobileNumber = "+61432869279",
            Email = "s.hariharan16@gmail.com",
            CardName = "H Satyavakeeswaran",
            //card details
            CardNumber = "4444333322221111",
            ExpiryMonth = "12",
            ExpiryYear = "25",
            CVN = "123",
            //transaction
            DonationAmount = 1000
        };

        protected DonationViewModel SecuredDonationViewModel => new DonationViewModel
        {
            // customer
            Title = "Mr.",
            FirstName = "Hariharan",
            LastName = "Satyavakeeswaran",
            MobileNumber = "+61432869279",
            Email = "s.hariharan16@gmail.com",
            CardName = "H Satyavakeeswaran",
            //secured card details
            SecuredCardData = "secured",
            //transaction
            DonationAmount = 1000
        };

        protected Transaction UnsecuredTransaction => UnsecuredDonationViewModel.ToTransaction();

        protected Transaction SecuredTransaction => SecuredDonationViewModel.ToSecureCardDataTransaction();

        protected string TransationSample => "{\r\n  \"Customer\": {\r\n    \"CardDetails\": {\r\n      \"Name\": \"John Smith\",\r\n      \"Number\": \"4444333322221111\",\r\n      \"ExpiryMonth\": \"12\",\r\n      \"ExpiryYear\": \"25\",\r\n      \"CVN\": \"123\"\r\n    },\r\n    \"Title\": \"Mr.\"\r\n  },\r\n  \"Payment\": {\r\n    \"TotalAmount\": 1000\r\n  },\r\n  \"Method\": \"ProcessPayment\",\r\n  \"TransactionType\": \"Purchase\"\r\n}";
    }
}
